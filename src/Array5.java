import java.util.Scanner;

public class Array5 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input arr[0]: ");
        int arr0 = sc.nextInt();
        arr[0] = arr0;
        System.out.print("Please input arr[1]: ");
        int arr1 = sc.nextInt();
        arr[1] = arr1;
        System.out.print("Please input arr[2]: ");
        int arr2 = sc.nextInt();
        arr[2] = arr2;
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);
        sc.close();
    }
    
}
