import java.util.Scanner;

public class MyApp {
    static int inputChoice() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3): ");
            int choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.print("Eror: Please input between 1-3");
        }

    }

    private static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static void printWelcome() {
        System.out.println("Welcome to my app !!!");
    }

    static void exitProgram() {
        System.out.print("Bye!!!");
        System.exit(0);

    }
    static int inputTime(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        return time;
    }
    static void printHelloWorld(){
        int time = inputTime();
        for( int i = 0 ; i < time ;i++){
            System.out.println("Hello World!!!");
        }
    }
    static void addTwoNumbers(){
        Scanner sc = new Scanner(System.in);
        int first,second;
        int result;
        System.out.print("Please input first number: ");
        first = sc.nextInt();
        System.out.print("Please input second number: ");
        second = sc.nextInt();
        result = add(first,second);
        System.out.println("Result = " + result);
    }
    static int add(int first,int second){
        int result = first + second;
        return result;
    }
    static void process(int choice){
        switch (choice) {
            case 1:
                printHelloWorld();
                break;
            case 2:
                addTwoNumbers();
                break;
            case 3:
                exitProgram();
                break;
    }
    }
    public static void main(String[] args) {
        int choice = 0;
        while (true) {
            printWelcome();
            printMenu();
            choice = inputChoice();
            process(choice);
            }
    }
}
