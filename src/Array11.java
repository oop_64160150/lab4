import java.util.Arrays;
import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        int arr[] = new int[5];
        int arr1[] = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
            arr1[i] = arr[i];
        }
        for (int i = 0; i < arr.length; i++) {
            Arrays.sort(arr1);
            System.out.println(arr1[i]);
        }
        System.out.println("Array");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        while (true) {
            Scanner sc = new Scanner(System.in);
            for (int i = 0; i < arr.length; i++) {
                System.out.print("Please input index: ");
                int index1 = sc.nextInt();
                int index2 = sc.nextInt();
                int temp = arr[index1];
                arr[index1] = arr[index2];
                arr[index2] = temp;
                break;
            }
            System.out.println();
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
            int n = 0;
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == arr1[i]) {
                    n += 1;
                }
            }
            if (n == 5) {
                System.out.println("Your win!!!");
                break;
            }
            sc.close();
        }
    }
}
