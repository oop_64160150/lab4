import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int size = 0;
        int unqSize = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        size = sc.nextInt();
        int arr[] = new int[size];
        for(int i =  0; i < arr.length;i++){
            System.out.print("Element " + i + ": ");
            int temp = sc.nextInt();
            int index =-1;
            for(int j=0;j<unqSize;j++){
                if(arr[j] == temp){
                    index = j;
                }
            } 
            if(index<0){
                arr[unqSize]=temp;
                unqSize++; 
            }
        }
        System.out.print("arr = ");
        for(int i = 0 ; i < unqSize ;i++){
            System.out.print(arr[i] + " ");
        }
        sc.close();
    }
}
