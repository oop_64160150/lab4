import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input arr[0]: ");
        int arr0 = sc.nextInt();
        arr[0] = arr0;
        System.out.print("Please input arr[1]: ");
        int arr1 = sc.nextInt();
        arr[1] = arr1;
        System.out.print("Please input arr[2]: ");
        int arr2 = sc.nextInt();
        arr[2] = arr2;
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.print("Please input search value: ");
        int value = sc.nextInt();
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (value == arr[i]) {
                index = i;
                System.out.println("found at index: " + index);
                break;
            }else if (value != arr[i]){
                index = -1;
            }
        }
        if (index<0){
            System.out.println("Not found");
            }
        sc.close();
    }
}
